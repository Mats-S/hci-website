## Website project | HCI
A simple and responsive site that works well on both mobile and desktop browsers. 
Built mainly with Bootstrap and jQuery.

# Screenshots
<div align="center">
Firefox browser:

![](MainPage.png)


Google Pixel 2:

![](MainPageMobile.png)


IPhone X:

![](MainPageMobile.png)

</div>